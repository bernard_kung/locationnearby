package com.telstra.locationnearby;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;


public class MainActivity extends FragmentActivity implements LocationListener,OnInfoWindowClickListener,ImageGetter{
	public final static String REF_ID = "in.wptrafficanalyzer.locationnearby.MESSAGE";
	public final static String GOOGLE_WEB_API="AIzaSyCn7i6kEGilwls43xc6vxsN2iY-AmeVrZc";
	
	GoogleMap mGoogleMap;	
	Spinner mSprPlaceType;
	ListView listview,listsearch;
	EditText inputSearch;
	
	// Search Listview Adapter
	ArrayAdapter<String> searchlistadapter;
	// ArrayList for Search Listview
    ArrayList<HashMap<String, String>> searchList;
	
	String[] mPlaceType=null;
	String[] mPlaceTypeName=null;
	
	double mLatitude=0;
	double mLongitude=0;
	
	boolean currentmap=true; //true=map, false=list
	
	ArrayAdapter<Spanned> locationadapter;
	Spanned[] locationvalues;
	String[] markername;
	String[] markervicinity;
	String[] markerreferenceid;
	String[] markerrating;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);		
		
		// Array of place types
		mPlaceType = getResources().getStringArray(R.array.place_type);
		
		// Array of place type names
		mPlaceTypeName = getResources().getStringArray(R.array.place_type_name);
		
		// Creating an array adapter with an array of Place types
		// to populate the spinner
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, mPlaceTypeName);
		
		// Getting reference to the Spinner 
		mSprPlaceType = (Spinner) findViewById(R.id.spr_place_type);
		
		// Setting adapter on Spinner to set place types
		mSprPlaceType.setAdapter(adapter);
		
		// Getting reference to the SupportMapFragment
    	final SupportMapFragment fragment = ( SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
					
		// Getting reference to Find Button
    	Button btnFind;
		btnFind = ( Button ) findViewById(R.id.btn_find);
		
		
		// Getting reference to List Button
		final Button btnList;
		btnList = ( Button ) findViewById(R.id.btn_list);
		
		
		// Getting reference to Search Button
		final Button btnSearch;
		btnSearch = ( Button ) findViewById(R.id.btn_search);
		
		//ListView for location results in list
		listview = (ListView) findViewById(R.id.locationlist);
		listview.setVisibility(View.INVISIBLE);
		
		//Input search (Edit Text) field
		inputSearch=(EditText)findViewById(R.id.inputSearch);
		inputSearch.setVisibility(View.INVISIBLE);
		inputSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		    @Override
		    public void onFocusChange(View v, boolean hasFocus) {
		        if (!hasFocus) {
		        	InputMethodManager inputMgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		        	inputMgr.hideSoftInputFromWindow(inputSearch.getWindowToken(), 0);
		        }
		    }
		});
		
		// Prepare data to populate the suburbs and postcodes in list search
        final ArrayList<String> locations = new ArrayList<String>();
		try{
        
        InputStream locationFile = MainActivity.this.getAssets().open("suburbs.txt");
    	BufferedReader locationreader = new BufferedReader(new InputStreamReader(locationFile, "UTF-8"));
    	
        String line = "";
        
        while( ( line = locationreader.readLine())  != null){
        	locations.add(line);   
        }
        
        locationreader.close();	
        
		}catch(Exception e){
           System.out.println(e.toString());
		}
		
		//List of all Australia suburbs and postcodes
		listsearch=(ListView) findViewById(R.id.list_search);
		listsearch.setVisibility(View.INVISIBLE);
		
		listsearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
            	
            	inputSearch.clearFocus();            	
            	//System.out.println(searchlistadapter.getItem(position).toString());
            	
            	try {
					SearchLocation sl=new SearchLocation(searchlistadapter.getItem(position).toString(),MainActivity.this);
					if(sl.getLat()!=0 && sl.getLong()!=0)
					{					
						
						mLatitude=sl.getLat();
						mLongitude=sl.getLong();
						mGoogleMap.clear();
						CameraUpdate center=CameraUpdateFactory.newLatLng(new LatLng(mLatitude,mLongitude));
						CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);

						mGoogleMap.moveCamera(center);
						mGoogleMap.animateCamera(zoom);
						
						getSupportFragmentManager().beginTransaction().show(fragment).commit();
						listview.setVisibility(View.INVISIBLE);
						listsearch.setVisibility(View.INVISIBLE);
						inputSearch.setVisibility(View.INVISIBLE);
						getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
						currentmap=true;
						
					}
					
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	
            }
		});
		
		
        // Adding items to listsearch
        searchlistadapter = new ArrayAdapter<String>(this, R.layout.maplist_row, R.id.rowTextView, locations);
        listsearch.setAdapter(searchlistadapter);
        
        
        
        /**
         * Enabling Search Filter for listsearch
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {
             
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                MainActivity.this.searchlistadapter.getFilter().filter(cs);   
            }
             
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3) {
                // TODO Auto-generated method stub
                 
            }
             
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub                          
            }
        });
        
        
        
        
        
		// Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available

        	int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        }else { // Google Play Services are available
        	
	    	
	    			
	    	// Getting Google Map
	    	mGoogleMap = fragment.getMap();
	    			
	    	// Enabling MyLocation in Google Map
	    	mGoogleMap.setMyLocationEnabled(true);
	    	
	    	mGoogleMap.setOnCameraChangeListener(new OnCameraChangeListener() {
	            @Override
	            public void onCameraChange(CameraPosition position) {
	                LatLngBounds bounds = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;
	                LatLng newlocation=bounds.getCenter();
	                mLatitude=newlocation.latitude;
	                mLongitude=newlocation.longitude;
	                System.out.println(newlocation.latitude+","+newlocation.longitude);
	            }
	        });
	    	
	    	//Detect on click for info window
	    	mGoogleMap.setOnInfoWindowClickListener(this);
	    	
	    	// Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            
            //300000=update every 5 minutes
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 300000, 0, this);

           /*
            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();

            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);       
            
            // Getting Current Location From GPS*/
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER); 
            
         	// create class object
            /*GPSTracker gps = new GPSTracker(this);

            // check if GPS enabled
            
            if(gps.canGetLocation()){
                 
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                
                location.setLatitude(latitude);
                location.setLongitude(longitude);
                
                //print out
                System.out.println("Your Location is - \nLat: " + latitude + "\nLong: " + longitude); 
                
            }else{
            	
                // can't get location
                // GPS  is not enabled
                // Ask user to enable GPS in settings
                gps.showSettingsAlert();
                
            }
            */
            
            if(location!=null){
                    onLocationChanged(location);                    
            }
            
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 300000, 0, this);
            

            
	    	
	    	// Setting click event lister for the find button
			btnFind.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {	
					
					
					int selectedPosition = mSprPlaceType.getSelectedItemPosition();
					String type = mPlaceType[selectedPosition];
					StringBuilder sb;
					
					if(!type.equals("tbuilding"))
					{
					sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
					sb.append("location="+mLatitude+","+mLongitude);
					sb.append("&radius=500"); //radius of 500 meters of current location / map location
					sb.append("&types="+type); //type of location search, eg. restaurant
					sb.append("&sensor=true"); //using current location
					sb.append("&key="+GOOGLE_WEB_API); //google web api (not google map android api)
					}
					else
					{
						sb = new StringBuilder("tbuilding.json");	//take tbuilding.json if search type is "Telstra Buildings"										
					}
					
					System.out.println(sb);
					
					// Creating a new non-ui thread task to download Google place json data 
			        PlacesTask placesTask = new PlacesTask();		        			        
			        
					// Invokes the "doInBackground()" method of the class PlaceTask
			        placesTask.execute(sb.toString());
					
					
				}
			});
	    	
			
			
						// Setting click event lister for the find button
						btnList.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {	
								
								inputSearch.clearFocus();
								
								if(currentmap==true)
								{
									btnList.setText("Map");
									getSupportFragmentManager().beginTransaction().hide(fragment).commit();
									listview.setVisibility(View.VISIBLE);
									listsearch.setVisibility(View.INVISIBLE);
									inputSearch.setVisibility(View.INVISIBLE);
									getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
									currentmap=false;
								}
								else
								{
									btnList.setText("List");
									getSupportFragmentManager().beginTransaction().show(fragment).commit();
									listview.setVisibility(View.INVISIBLE);
									listsearch.setVisibility(View.INVISIBLE);
									inputSearch.setVisibility(View.INVISIBLE);
									getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
									currentmap=true;
									
								}
							}
						});

						
						// Setting click event lister for the find button
						btnSearch.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {	
								
								getSupportFragmentManager().beginTransaction().hide(fragment).commit();
								listview.setVisibility(View.INVISIBLE);
								listsearch.setVisibility(View.VISIBLE);
								inputSearch.setVisibility(View.VISIBLE);
								currentmap=false;
							}
						});						
			
			
        }		
 		
	}
	
	/** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        String thisdata="";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        String nextpage="";
        String downloadstatus="";
        
        //for any remote API request for location search
        if(strUrl.contains("http"))
        {
	        try{
	                URL url = new URL(strUrl);                
	                
	
	                // Creating an http connection to communicate with url 
	                urlConnection = (HttpURLConnection) url.openConnection();                
	
	                // Connecting to url 
	                urlConnection.connect();                
	
	                // Reading data from url 
	                iStream = urlConnection.getInputStream();
	
	                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
	
	                StringBuffer sb  = new StringBuffer();
	
	                String line = "";
	                while( ( line = br.readLine())  != null){
	                        sb.append(line);
	                }
	
	                thisdata = sb.toString();
	                
	                //System.out.println(data);
	
	                br.close();
	                iStream.close();
	                urlConnection.disconnect();
	                
	                
	                //Check if got next page
	                JSONObject jObject = new JSONObject(thisdata);
	                if(!jObject.isNull("status"))	                
		            downloadstatus = jObject.getString("status");
	                
	                System.out.println(downloadstatus);
	                
	                if(downloadstatus.equalsIgnoreCase("OK"))
	                {
		                if(!jObject.isNull("next_page_token"))	                
		                nextpage = jObject.getString("next_page_token");
		                	                
		                nextpage="";
		                
		                //Extract the "results" JSON
		                JSONArray resultArray=jObject.getJSONArray("results");
	
		                for (int i=0;i<resultArray.length();i++)
		                {
		                	JSONObject jt = resultArray.getJSONObject(i);
		                	data+=","+jt.toString();
		                }
		                
		                data=data.substring(1);
		                
		                
		                //Getting more results from Google Nearby search, max 60 records can be returned in 3 Pages
		                while(nextpage!="")
		                {
		                		                	
		                	SystemClock.sleep(1500);
		                	
		                	System.out.println("new page!");
		                	
		                	url = new URL("https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken="+nextpage+"&sensor=true&key="+GOOGLE_WEB_API);
		                	
		                	System.out.println(url);
	
			                
		                	// Creating an http connection to communicate with url 
			                urlConnection = (HttpURLConnection) url.openConnection();                
			
			                // Connecting to url 
			                urlConnection.connect();                
			
			                // Reading data from url 
			                iStream = urlConnection.getInputStream();
			
			                br = new BufferedReader(new InputStreamReader(iStream));
			
			                sb  = new StringBuffer();
			
			                line = "";
			                while( ( line = br.readLine())  != null){
			                        sb.append(line);
			                }
			
			                thisdata = sb.toString();
			                
			                //System.out.println(thisdata);
			
			                br.close();  
			                
			                jObject = new JSONObject(thisdata);
			                
			                resultArray=jObject.getJSONArray("results");
	
			                for (int i=0;i<resultArray.length();i++)
			                {
			                	JSONObject jt = resultArray.getJSONObject(i);
			                	data+=","+jt.toString();
			                }
			                
			                
			                if(!jObject.isNull("next_page_token"))
			                {
			                	nextpage = jObject.getString("next_page_token");
			                }
			                else
			                { 
			                	nextpage="";
			                }
			                
			                
		                }
		                
		                //All (up to) three pages are now combined into one JSON string
		                data="{"+"\"results\" : ["+data+"]}";
		                //System.out.println(data);
	                }
	                else
	                {
	                	System.out.println("no results1");
	                }
	
	        }catch(Exception e){
	                Log.d("Exception while downloading url", e.toString());
	        }finally{
	                iStream.close();
	                urlConnection.disconnect();
	        }
        
        }
        else
        {
        	//For any local based JSON locations such as Telstra Buildings
        	
        	InputStream jsonFile = MainActivity.this.getAssets().open(strUrl);
        	BufferedReader reader = new BufferedReader(new InputStreamReader(jsonFile, "UTF-8"));
        	StringBuffer sb  = new StringBuffer();
        	
            String line = "";
            while( ( line = reader.readLine())  != null){
                    sb.append(line);
            }
            data = sb.toString();
            reader.close();	
        }

        return data;
    }         

	
	/** A class, to download Google Places */
	private class PlacesTask extends AsyncTask<String, Integer, String>{

		String data = null;
		
		// Invoked by execute() method of this object
		@Override
		protected String doInBackground(String... url) {
			try{
				data = downloadUrl(url[0]);
			}catch(Exception e){
				 Log.d("Background Task",e.toString());
			}
			return data;
		}
		
		// Executed after the complete execution of doInBackground() method
		@Override
		protected void onPostExecute(String result){			
			ParserTask parserTask = new ParserTask();
			
			// Start parsing the Google places in JSON format
			// Invokes the "doInBackground()" method of the class ParseTask
			parserTask.execute(result);
		}
		
	}
	
	/** A class to parse the Google Places in JSON format */
	private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

		JSONObject jObject;
		
		// Invoked by execute() method of this object
		@Override
		protected List<HashMap<String,String>> doInBackground(String... jsonData) {
		
			List<HashMap<String, String>> places = null;			
			PlaceJSONParser placeJsonParser = new PlaceJSONParser();
        
	        try{
	        	
	        	if(!jsonData[0].isEmpty())
	        	{
	        		jObject = new JSONObject(jsonData[0]);
	        	
	            /** Getting the parsed data as a List construct */
	            places = placeJsonParser.parse(jObject);
	        	}
	            
	        }catch(Exception e){
	                Log.d("Exception",e.toString());
	        }
	        return places;
		}
		
		// Executed after the complete execution of doInBackground() method
		@Override
		protected void onPostExecute(List<HashMap<String,String>> list){			
			

			try{
			// Clears all the existing markers 
			mGoogleMap.clear();
			
			locationvalues=new Spanned[list.size()];
			markername=new String[list.size()];
			markervicinity=new String[list.size()];
			markerreferenceid=new String[list.size()];
			markerrating=new String[list.size()];
			
			
			for(int i=0;i<list.size();i++){
			
				// Creating a marker
	            MarkerOptions markerOptions = new MarkerOptions();
	            
	            // Getting a place from the places list
	            HashMap<String, String> hmPlace = list.get(i);
	
	            // Getting latitude of the place
	            double lat = Double.parseDouble(hmPlace.get("lat"));	            
	            
	            // Getting longitude of the place
	            double lng = Double.parseDouble(hmPlace.get("lng"));
	            
	            // Getting name
	            final String name = hmPlace.get("place_name");
	            	           
	            //Get reference id
	            final String referenceid=hmPlace.get("referenceid");
	            
	            // Getting vicinity
	            final String vicinity = hmPlace.get("vicinity");
	            
	            // Getting rating
	            final String rating = hmPlace.get("rating");
	            
	            
	            //Settings in array
	            markername[i]=name;
				markervicinity[i]=vicinity;
				markerreferenceid[i]=referenceid;
				markerrating[i]=rating;
				
				int starint;
				float stardec;
				String starimg="";
				String wholestar;
				String halfstar;
				
				
				if(rating!="")
				{
					starimg="<br/>";
					starint=(int) Double.parseDouble(rating);
					stardec=(float) (Double.parseDouble(rating) % 1);
					wholestar="<img src=\"wholestar.png\">";
					halfstar="<img src=\"halfstar.png\">";
					for(int k=0;k < starint; k++)
					{
						starimg+=wholestar;						
					}
					
					if(stardec>=0.5)
					starimg=starimg+halfstar;
					
					starimg+="<i><small>"+rating+"</small></i>";
				}
	            
	            
	            LatLng latLng = new LatLng(lat, lng);
	            
	            // Setting the position for the marker
	            markerOptions.position(latLng);
	
	            // Setting the title for the marker. 
	            //This will be displayed on taping the marker
	            markerOptions.title(referenceid);
	            
	            markerOptions.snippet(Integer.toString(i));
	            
	            locationvalues[i]=Html.fromHtml("<b>"+ name + "</b><br/>" + vicinity+starimg,MainActivity.this, null);
	
	            // Placing a marker on the touched position
	            mGoogleMap.addMarker(markerOptions);            
        
			}
			
			
			/*----------------------*/   
            
	         // Setting a custom info window adapter for the google map
				mGoogleMap.setInfoWindowAdapter(new InfoWindowAdapter() {
		 
		            // Use default InfoWindow frame
		            @Override
		            public View getInfoWindow(Marker arg0) {
		                return null;
		            }
		 
		            // Defines the contents of the InfoWindow
		            @Override
		            public View getInfoContents(Marker arg0) {
		 
		                // Getting view from the layout file info_window_layout
		                View v = getLayoutInflater().inflate(R.layout.info_window, null);
		 
		                // Getting reference to the TextView to set latitude
		                TextView tvLat = (TextView) v.findViewById(R.id.infowindow_content);
		                
		                Integer thismarkerid=Integer.parseInt(arg0.getSnippet());
		                
		                String infoname="<b>"+markername[thismarkerid] + "</b><br/>"  + markervicinity[thismarkerid];
		                
		                String inforating;
		                if(markerrating[thismarkerid]!="")		                
		                inforating="<br><font color=red><b>Rating:<b/>"+ markerrating[thismarkerid]+"</font>";
		                else
		                inforating="";
		                
		                String inforef;
		                if(markerreferenceid[thismarkerid]!="")	
		                inforef="<br/><br/>"+"<a href='http://"+markerreferenceid[thismarkerid]+"'>Click for Details</a>";
		                else
		                inforef="";
		                
		
		                // Setting the latitude
		                tvLat.setText(Html.fromHtml(infoname+inforating+inforef));
		 
		                // Returning the view containing InfoWindow contents
		                return v;
		 
		            }
		            
		            
		            
		            
		        });
				/*----------------------*/ 
			
			
			locationadapter= new ArrayAdapter<Spanned>(MainActivity.this,R.layout.maplist_row,locationvalues);
			listview.setAdapter(locationadapter);
			
			
			// ListView Item Click Listener
	        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	              @Override
	              public void onItemClick(AdapterView<?> parent, View view,int position, long id) {

	            	  System.out.println(markerreferenceid[position]);
	            	  
	            	  if(!markerreferenceid[position].isEmpty())
	            	  {  
	            	  Intent intent = new Intent(getBaseContext(), DetailLocation.class); 
	                  intent.putExtra(REF_ID, markerreferenceid[position]);
	                  startActivity(intent);	  
	            	  }
	            	  else
	            	  {
	            		  Toast.makeText(MainActivity.this, "No detail page for this location", Toast.LENGTH_SHORT).show();
	            	  }

	              }
	        });
			
		}catch(Exception e)
		{
			System.out.println(e.toString());
			Toast.makeText(MainActivity.this, "No Results found in this area", Toast.LENGTH_SHORT).show();
			
		}
			
			
	  }
		
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onLocationChanged(Location location) {
		mLatitude = location.getLatitude();
		mLongitude = location.getLongitude();
		LatLng latLng = new LatLng(mLatitude, mLongitude);
		
		//mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
		//mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
		//mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f),4000,null);
		//mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), 15.5f), 4000, null);
		
		
		CameraUpdate center=CameraUpdateFactory.newLatLng(new LatLng(mLatitude,mLongitude));
		CameraUpdate zoom=CameraUpdateFactory.zoomTo(15);

		mGoogleMap.moveCamera(center);
		mGoogleMap.animateCamera(zoom);
		
		
		System.out.println(mLatitude+","+mLongitude);
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	 public void onInfoWindowClick(Marker marker) {
	  
		String thismarkerref=marker.getTitle().trim();
		
		System.out.println(thismarkerref);
	  
	  if(!thismarkerref.isEmpty())
	  {  
		  Intent intent = new Intent(getBaseContext(), DetailLocation.class); 
          intent.putExtra(REF_ID, marker.getTitle());
          startActivity(intent);	  		  		  		  
	  }
	  else
	  {
		  Toast.makeText(MainActivity.this, "No detail page for this location", Toast.LENGTH_SHORT).show();
	  }
	  
	  
	 }
	
	
	@Override
    public Drawable getDrawable(String arg0) {
        // TODO Auto-generated method stub
        int id = 0;

        if(arg0.equals("wholestar.png")){
            id = R.drawable.wholestar;
        }

        if(arg0.equals("halfstar.png")){
            id = R.drawable.halfstar;
        }
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = getResources().getDrawable(id);
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

        return d;
    }
	
	
	
}
