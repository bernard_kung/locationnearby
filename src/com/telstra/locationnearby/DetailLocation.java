package com.telstra.locationnearby;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class DetailLocation extends Activity{
	String referenceid;
	private static final String MAP_URL = "file:///android_asset/locationdetails.html";
	private final static String GOOGLE_WEB_API="AIzaSyCn7i6kEGilwls43xc6vxsN2iY-AmeVrZc";
	private WebView webView2;

	String name="";
	String address="";
	String phone="";
	String website="";
	String googleplus="";
	String photoref="";
	String rating="";
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
	super.onCreate(savedInstanceState);

	Intent intent = getIntent();
	referenceid = intent.getStringExtra(MainActivity.REF_ID);

	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	StrictMode.setThreadPolicy(policy);

	setContentView(R.layout.location_details);

	requestWebService("https://maps.googleapis.com/maps/api/place/details/json?reference="+referenceid+"&sensor=true&key="+GOOGLE_WEB_API);
	}


	@SuppressLint("JavascriptInterface")
	public void requestWebService(String serviceUrl) {
	    //disableConnectionReuseIfNecessary();
		String allresults=null;
	 
	    HttpURLConnection urlConnection = null;
	    try {
	        // create connection
	    	
	    	URL urlToRequest = new URL(serviceUrl);
	        urlConnection = (HttpURLConnection) 
	            urlToRequest.openConnection();
	        urlConnection.setConnectTimeout(5000);
	        urlConnection.setReadTimeout(5000);
	         
	        // handle issues
	        int statusCode = urlConnection.getResponseCode();
	        if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
	            // handle unauthorized (if service requires user login)
	        } else if (statusCode != HttpURLConnection.HTTP_OK) {
	            // handle any other errors, like 404, 500,..
	        }
	         
	        // create JSON object from content
	        //InputStream in = new BufferedInputStream(urlConnection.getInputStream());
	        
	        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
	        
	        String line = null;
	        StringBuilder sb = new StringBuilder();
	        
	        while((line = in.readLine()) != null) {
	          //System.out.println(line);
	          sb.append(line + "\n");
	        }
	        allresults = sb.toString();
	        
	        //System.out.println(allresults);
	        
	        JSONObject jObject = new JSONObject(allresults).getJSONObject("result");	        
	        
	        name = jObject.getString("name");
	        name = name.replaceAll("'", "\\\\'");
	        
	        if(!jObject.isNull("formatted_address"))
	        address=jObject.getString("formatted_address");
	        address=address.replaceAll("'", "\\\\'");
	        
	        if(!jObject.isNull("formatted_phone_number"))
	        phone=jObject.getString("formatted_phone_number");
	        
	        if(!jObject.isNull("rating"))
	        rating=jObject.getString("rating");
	        
	        if(!jObject.isNull("website"))
	        website=jObject.getString("website");
	        
	        
	        if(!jObject.isNull("photos"))
	        {	
		        if(!jObject.getJSONArray("photos").getJSONObject(0).isNull("photo_reference"))
		        {
		        	photoref = jObject.getJSONArray("photos").getJSONObject(0).getString("photo_reference");
		        }
	        }
	        
	        googleplus=jObject.getString("url");
	        
	        System.out.println(name);
	        System.out.println(address);
	        System.out.println(phone);
	        System.out.println(rating);
	        System.out.println(website);
	        System.out.println(googleplus);
	        

	        
	        webView2 = (WebView) findViewById(R.id.webview2);
	        webView2.getSettings().setJavaScriptEnabled(true);//Webview JavaScript
	        webView2.setWebViewClient(new WebViewClient() {
	            @Override
	            public boolean shouldOverrideUrlLoading(WebView view, String url) {
	                if (url.startsWith("tel:")) { 
	                        Intent intent = new Intent(Intent.ACTION_DIAL,
	                                Uri.parse(url)); 
	                        startActivity(intent); 	                	               
	                }else if(url.startsWith("http:") || url.startsWith("https:")) {
	                	view.getContext().startActivity(
	    	                    new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
	                	
	                	//view.loadUrl(url);
	                }
	                return true;
	            }	           
	            
	        });
	        
	        webView2.loadUrl("javascript:"+
	        		"var name='"+name+"'; " +
	        		"var address='"+address+"'; " +
	        		"var phone='"+phone+"'; " +
	        		"var rating='"+rating+"'; " +
	        		"var googleplus='"+googleplus+"'; " +
	        		"var photoref='"+photoref+"'; " +
	        		"var website='"+website+"';");
	        
	        
	        webView2.loadUrl(MAP_URL);  //URL 
	        
	        
	        
	        
	        

	    } catch (MalformedURLException e) {
	    	System.out.println("======================URL is invalid======================");
	        // URL is invalid
	    } catch (SocketTimeoutException e) {
	    	System.out.println("======================Data retrieval or connection timed out======================");
	        // data retrieval or connection timed out
	    } catch (IOException e) {
	    	System.out.println("======================Could not read response body======================");
	        // could not read response body 
	        // (could not create input stream)
	    } catch (JSONException e) {
	    	System.out.println("======================JSON error======================");
	    	System.out.println(e.toString());
	        // response body is no valid JSON string
	    } finally {
	        if (urlConnection != null) {
	            urlConnection.disconnect();
	        }
	    }       
	}




	}
