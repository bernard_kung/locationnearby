package com.telstra.locationnearby;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.os.StrictMode;
import android.widget.Toast;


public class SearchLocation {
	
	double newLatitude=0;
	double newLongitude=0;
	String thisdata="";
	
	
	public SearchLocation(String tosearch,Context context) throws IOException
	{
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
		
		InputStream iStream = null;
        HttpURLConnection urlConnection = null;
		
        tosearch=tosearch.replaceAll(" ", "+");
		
        try{
		URL url = new URL("http://maps.googleapis.com/maps/api/geocode/json?address="+tosearch+",+AU&sensor=true");
		System.out.println(url);
        
        // Creating an http connection to communicate with url 
        urlConnection = (HttpURLConnection) url.openConnection();                

        // Connecting to url 
        urlConnection.connect();                

        // Reading data from url 
        iStream = urlConnection.getInputStream();

        BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

        StringBuffer sb  = new StringBuffer();

        String line = "";
        while( ( line = br.readLine())  != null){
                sb.append(line);
        }
        thisdata = sb.toString();
        
      //Check if got next page
        JSONObject jObject = new JSONObject(thisdata);
 
        String nLatitude = jObject.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getString("lat");
        String nLongitude = jObject.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getString("lng");	
        
        newLatitude=Double.parseDouble(nLatitude);
        newLongitude=Double.parseDouble(nLongitude);
        
        System.out.println(newLatitude);
        System.out.println(newLongitude);
        
        
        }catch(Exception e)
        {
        	System.out.println(e.toString());
        	Toast.makeText(context, "No Search Results", Toast.LENGTH_SHORT).show();
        }finally{
        	iStream.close();
            urlConnection.disconnect();
        }
		
		
	}
	
	
	public double getLat()
	{
		return newLatitude;
	}
	
	public double getLong()
	{
		return newLongitude;
	}
	
	
}
